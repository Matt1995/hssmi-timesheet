// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://sl.hssmi.org/ProjectTimecard/MyAssignments
// @grant        GM.setValue
// @grant        GM.getValue
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js
// ==/UserScript==

class TA {

    static log(str) {

        console.log("[TA] :: " + str.toString())

    }

    static clear() {

        GM.setValue("row", -1)
        GM.setValue("csv", "")

    }

}

class CSVRow {

    /**
     * Construct a new CSVRow from a split line.
     *
     * @param {string} string The row data as a single
     * string (usually split by \n)
     */
    constructor(string) {

        this.data = string
        this.cols = string.split(",")

    }

    /**
     * Return a column or the default value.
     *
     * @param {int} col The column id to fetch
     * @param {*} iff The conditonal
     * @param {*} thenn Returned if col == iff is true.
     */
    getColDefault(col, iff, thenn) {

        return this.cols[col] == iff ? thenn : this.cols[col]

    }

    /**
     * Does this row refer to a period in which it is under
     * "ALL"?
     */
    isAllPeriod() {
        return this.period == "ALL"
    }

    /**
     * Fetch the period part of the csv row.
     */
    get period() {
        return this.cols[0]
    }

    /**
     * Fetch the project part of the csv row.
     */
    get project() {
        return this.cols[1]
    }

    /**
     * Fetch the package part of the csv row.
     */
    get package() {
        return this.cols[2]
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get monday() {
        return parseFloat(this.getColDefault(3, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get tuesday() {
        return parseFloat(this.getColDefault(4, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get wednesday() {
        return parseFloat(this.getColDefault(5, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get thursday() {
        return parseFloat(this.getColDefault(6, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get friday() {
        return parseFloat(this.getColDefault(7, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get saturday() {
        return parseFloat(this.getColDefault(8, "", "0"))
    }

    /**
     * Fetch this day's time or "0" if N/A
     */
    get sunday() {
        return parseFloat(this.getColDefault(9, "", "0"))
    }

    /**
     * Fetch the assigned part of this row.
     */
    get assigned() {
        return this.getColDefault(10, "", "FALSE")
    }

    /**
     * Fetch the billable part of this row.
     */
    get billable() {
        return this.getColDefault(11, "", "TRUE")
    }

}

class CSVFile {

    /**
     * Read a raw file as a string into a CSVFile object.
     *
     * @param {string} string The raw file data of the csv file.
     */
    constructor(string) {

        this.data = string
        this.rows = []

        let lines = string.split("\n")

        for (let i = 0; i < lines.length; i++) {

            this.rows[this.rows.length] = new CSVRow(lines[i])

        }

    }

    /**
     *
     * Return all rows for the given period. All is also
     * returned.
     *
     * @param {string} str The period in format dd/mm/yyyy
     */
    getRowsForPeriod(str) {

        let rows = []
        for (let i = 0; i < this.rows.length; i++) {

            let row = this.rows[i]

            if (row.isAllPeriod() || row.period == str) rows[rows.length] = row


        }

        return rows

    }

    getNextValidPeriodFrom(i) {

        for (let j = i + 1; j < this.rows.length; j++) {

            let row = this.rows[j]
            if (row.period == "PERIOD" || row.period == "") continue

            return j

        }

        return -1

    }

}

class Project {

    /**
     * Create a new project with the given information
     *
     * @param {number} projectId The ID of the project (such
     * as R551)
     * @param {string} projectTask The task of the project
     * a* (such as "WP01" )
     * @param {string} endPeriod The end period in format
     * DD/MM/YYYY
     */
    constructor(projectId, projectTask, endPeriod) {

        this.id = projectId
        this.task = projectTask
        this.data = {}
        this.validated = false
        this.endPeriod = endPeriod
        this.moment = moment(endPeriod, "DD/MM/YYYY")

        this.days = []
        for (let i = 0; i < 7; i++) this.days[i] = 0

    }

    get monday() { return this.days[0] }
    get tuesday() { return this.days[1] }
    get wednesday() { return this.days[2] }
    get thursday() { return this.days[3] }
    get friday() { return this.days[4] }
    get saturday() { return this.days[5] }
    get sunday() { return this.days[6] }

    set monday(n) { this.days[0] = n }
    set tuesday(n) { this.days[1] = n }
    set wednesday(n) { this.days[2] = n }
    set thursday(n) { this.days[3] = n }
    set friday(n) { this.days[4] = n }
    set saturday(n) { this.days[5] = n }
    set sunday(n) { this.days[6] = n }

    hoursFromCSVRow( r ) {

        this.monday = r.monday
        this.tuesday = r.tuesday
        this.wednesday = r.wednesday
        this.thursday = r.thursday
        this.friday = r.friday
        this.saturday = r.saturday
        this.sunday = r.sunday

    }

    /**
     *
     * Validate this project with the server.
     *
     * @param {callback} success The function to invoke when
     * validation was successful.
     * @param {callback} fail The function to invoke when
     * validation was not successful.
     */
    validate(success, fail) {


        SL.makePostTo("./MyAssignmentsGridPopulate", { "PageNumber": "1", "Duplicate": "false" }, () => {

            // Seems you have to do this before it validates the
            // token for edit. Gives us a template to work with,
            // too.
            SL.makePostTo("./AssignmentHeader", { "TimecardDocNumber": "", "TimecardLineNumber": "NEW" }, (ret) => {

                this.data["main"] = ret

                // Now we can query the DB for the info on the
                // project :)
                SL.makePostTo("./AssignmentHeaderValidate", { "Result": this.id, "CalledBy": "Project" }, (idData) => {

                    if (idData[0].Valid == "error") {

                        alert("The project name you've provided (" + this.id + ") is not valid. (According to their server)")
                        if (fail) fail()

                    } else {

                        this.data["project"] = idData[0]

                        // And also query for the work package.
                        SL.makePostTo("./AssignmentHeaderValidate", { "Result": this.task, "CalledBy": "ProjectTask" }, (taskData) => {

                            if (taskData[0].Valid == "error") {

                                alert("The task name you've provided (" + this.task + ") for project (" + this.id + ") is not valid. (According to their server)")
                                if (fail) fail()

                            } else {

                                this.data["task"] = taskData[0]

                                // "Save" this project, which
                                // returns the UI element you
                                // see after adding a project.
                                // We don't really need the UI
                                // element, but this returns
                                // some important post
                                // information.
                                SL.makePostTo("./AssignmentHeaderSave", { "CalledBy": "New" }, (rowData) => {

                                    if (rowData[0].Valid == "error") {

                                        alert("It seems I couldn't save/validate (" + this.id + "). I'm not really sure why.")
                                        if (fail) fail()

                                    } else {

                                        this.data["row"] = rowData[0]["NewGridLine"]
                                        this.validated = true
                                        if (success) success(this)

                                    }

                                })

                            }

                        })

                    }

                })

            })

        })

    }

    /**
     * Return the most recent line number
     *
     * @param {callback} fn The callback to invoke on finish.
     */
    getLineNumber(fn) {

        SL.makePostTo("./AssignmentGridDayValidate", {

            "CalledBy": "TimecardPFTLines.DayOfWeekHours0",
            "Value": 0,
            "ColumnHeading": "DayOfWeekHours0",
            "DetailRows": [this.data.urow]

        }, (d) => {

            if (d[0].Valid == "error") {

                alert("Couldn't get line number.")
                console.log(d)

            } else {

                fn(d[0].GridLine.DailyHours[0].LineNumber)

            }

        })

    }

    /**
     * Format this project ready for uploading.
     *
     * @param {callback} fn The callback to invoke on finish
     */
    formatForUpload(fn) {

        if (this.validated == false) return

        this.data["urow"] = {}

        for (var key in this.data.row) this.data.urow[key] = this.data.row[key]

        this.data.urow["RowState"] = "Inserted"
        this.data.urow["ProjectTaskSubtask"] = this.data.urow["RowCaption"]

        delete this.data.urow["DailyHours"]
        delete this.data.urow["RowCaption"]

        this.data.urow["RowTotalHours"] = this.monday + this.tuesday + this.wednesday + this.thursday + this.friday + this.saturday + this.sunday

        let a = (n, hrs, y) => {

            this.data.urow["DayOfWeekHours" + n] = hrs
            this.data.urow["DayOfWeekLineNumber" + n] = y
            this.data.urow["DayOfWeekNoteID" + n] = 0
            this.data.urow["DayOfWeekIComment" + n] = 0
            this.data.urow["DayOfWeekDate" + n] = moment(this.moment).subtract(6 - n, "days").format("DD/MM/YYYY")

        }

        a(0, this.monday, 0)
        a(1, this.tuesday, 0)
        a(2, this.wednesday, 0)
        a(3, this.thursday, 0)
        a(4, this.friday, 0)
        a(5, this.saturday, 0)
        a(6, this.sunday, 0)

        this.getLineNumber((ln) => {

            a(0, this.monday, ln)
            a(1, this.tuesday, ln)
            a(2, this.wednesday, ln)
            a(3, this.thursday, ln)
            a(4, this.friday, ln)
            a(5, this.saturday, ln)
            a(6, this.sunday, ln)

            fn()

        })

    }

    /**
     * Validate a given day. Required by MS systems.
     *
     * @param {number} n The day of the week
     * @param {callback} fn The callback to invoke once the
     * validation has completed.
     */
    validateDay(n, fn) {

        if (this.days[n] == 0) return fn()

        SL.makePostTo("./AssignmentGridDayValidate", {

            "CalledBy": "TimecardPFTLines.DayOfWeekHours" + n,
            "Value": this.days[n],
            "ColumnHeading": "DayOfWeekHours" + n,
            "DetailRows": [this.data.urow]

        }, (ret) => {

            if (ret[0].Valid == "error") {

                alert("It seems that one of the working week numbers is wrong.")

            } else {

                fn()

            }

        })

    }

    /**
     *
     * Upload this project to the MS server. Make sure
     * you've validated this project first.
     *
     * @param {callback} fn The functon to invoke on upload
     * finish.
     */
    upload(fn) {

        this.formatForUpload(() => {

            console.log(this)
            let outerThis = this
            let iter = {

                i: -1,
                next: function () {
                    this.i = this.i + 1
                    this.fn()
                },
                fn: function () {

                    if (this.i < outerThis.days.length) {

                        console.log(outerThis)
                        console.log("Validing a day (" + outerThis.days[this.i] + ")")
                        //this.next()
                        outerThis.validateDay(this.i, () => { this.next() })

                    } else {

                        SL.makePostTo("./AssignmentGridSave", {

                            "DetailRows": [outerThis.data.urow]

                        }, (res) => {

                            console.log(res)

                            SL.makePostTo("./MyAssignmentsGridPopulate", { "PageNumber": "1", "Duplicate": "false" }, (ret) => {

                                console.log(ret)
                                fn()

                            })

                        })

                    }

                }

            }
            iter.next()

        })


    }

}

class SL {

    static get token() {

        return $('[name="__RequestVerificationToken"]').val()

    }

    static makePostTo(url, data, success, fail) {

        data["__RequestVerificationToken"] = SL.token

        var jqxhr = $.post(url, data, success).fail(fail)


    }


}


class TimecardPage {

    static injectCSVButton(onUpload) {

        let $browser = $("<input type=file style='position:absolute; opacity:0' id=csv-browse></input>")
        let $btn = $("<div id=csv-btn>From CSV..</div>")
        $btn.attr("style", "position:fixed; z-index:2000; background-color:white; padding:5px; border-radius:5px; right:100px; cursor:pointer;")

        $("body").append($browser)
        $("body").append($btn)

        $btn.on("click", () => {

            TA.log("Showing browse file dialogue..")
            $browser.trigger("click")

        })

        $browser.on("change", function (d) {

            let fr = new FileReader()
            fr.onload = function (da) {
                onUpload(da.target.result)
            }
            fr.readAsText(this.files[0])

        })

    }

    static get endingPeriod() {

        let $elm = $($("#Content .ui-body.ui-body-a")[1])
        let text = $elm.text().trim()
        let spl = text.split(" ")
        let upper = spl[spl.length - 1]

        return upper

    }

    static navigateToEndingPeriod(str) {

        ChangePeriod(str)

    }

}

$(document).ready(() => {

    if (TimecardPage.endingPeriod == "") {

        TA.clear()
        TA.log("I've reset my internals.")

    }

    GM.getValue("row", -1).then((i) => {

        if (i == -1) { return }
        if (i == -100) {

            alert("Done.")
            TA.clear()
            return

        }

        GM.getValue("csv", "").then((csvFile) => {

            let csv = new CSVFile(csvFile)

            // Are we in the right place?
            if (csv.rows[i].period != TimecardPage.endingPeriod) {
                TA.log("I can't run here, moving to: " + csv.rows[i].period)
                ChangePeriod(csv.rows[i].period)
                return
            }

            r = csv.rows[i]

            let proj = new Project(r.project, r.package, r.period)
            proj.validate((p) => {

                p.hoursFromCSVRow( r )

                p.upload(() => {

                    let next = csv.getNextValidPeriodFrom(i)
                    GM.setValue("row", next == -1 ? -100 : next)

                    // This is required as MS seem to tie
                    // the saving to an expected page
                    // reload. :/
                    ChangePeriod()

                })


            })

        })

    })


    TimecardPage.injectCSVButton((str) => {

        let csv = new CSVFile(str)

        GM.setValue("csv", str)
        TA.log(csv)

        let next = csv.getNextValidPeriodFrom(0)
        GM.setValue("row", next)

        if (next == -1) {
            alert("There do not seem to be any dates in that timesheet")
            TA.clear()
            return
        }

        TA.log("Going to: " + csv.rows[next].period)
        TimecardPage.navigateToEndingPeriod(csv.rows[next].period)

    })

})